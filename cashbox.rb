class Cashbox
  attr_reader :available_cash, :debit
  def self.available_nominals
    [ 0.05,
      0.1,
      0.25,
      0.5,
      1.0,
      2.0,
      5.0,
      10.0,
      20.0,
      50.0,
      100.0,
      200.0]
  end

  def initialize(cashbox_preload = {}, debit = 0.0)
    @available_cash = cashbox_preload.select { |k,v|
      Cashbox.available_nominals.include?(k) &&
      v.is_a?(Fixnum) && v > 0
    }
    @debit = debit
  end

  def payment_process(price)
    incoming(price)
  end

  def money_amount
    available_cash.map {|k,v| k*v}.sum
  end

  private

  def incoming(price)
    input =  0.0
    while (price - input - debit) > 0 do
      # amount is used to store value of input coins
      puts "Waiting for pay #{price - input - debit}"
      incoming_coin = gets.chomp
      if incoming_coin == "change"
        change(input) && return
      end
      value = incoming_coin.to_f
      if (Cashbox.available_nominals & [value]).any?
        available_cash[value] ||= 0
        available_cash[value] += 1
        input += value
      else
        puts "Unknown coin"
      end
    end
    change(input + debit - price)
    true
  end

  def available_coins
    available_cash.map {|k,v|
      if Cashbox.available_nominals.include?(k.to_f) && v > 0
        k
      end
    }.compact.sort.reverse
  end

  def take_coin_from_cash(coin_value)
    return unless available_coins.include?(coin_value.to_f)
    @available_cash[coin_value] -= 1
    true
  end

  def change(amount)
    available_coins.each do |coin_value|
      while (amount - coin_value) >= 0
        if take_coin_from_cash(coin_value)
          amount -= coin_value
          puts "returned #{coin_value}"
        else
          next
        end
      end
    end
    if amount > 0
      @debit = amount
      puts "Not enough cash to return #{debit}"
    end
  end
end
