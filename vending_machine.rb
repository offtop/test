require 'json'
class VendingMachine
  attr_reader :goods, :cashbox
  def initialize
    @goods = JSON.parse(File.read('./goods.json'))
    @cashbox = Cashbox.new
  end

  def run
    while true do
      puts "==============="
      product_id = select_product
      product = goods[product_id]
      deliver_product(product_id) if cashbox.payment_process(product["price"].to_f)
      puts "==============="
    end
  end

  private
  def debit
    cashbox.debit
  end

  def print_goods
    puts "Debit: #{debit}"
    puts "Choose product:"
    goods.each_with_index do |good,index|
      next unless product_available?(index)
      puts " - #{index} : #{good["name"]}: #{good["price"]}"
    end

  end

  def product_available?(index)
    product = goods[index]
    return unless product
    return unless product["name"]
    qty = product["qty"]
    return if (qty && qty.to_i <= 0)
    return if !product["price"]
    true
  end

  def select_product
    while true do
      print_goods
      selected_product_id = gets.chomp.to_i
      return selected_product_id if product_available?(selected_product_id)
      puts "Product is not available"
    end
  end

  def deliver_product(id)
    puts "Product delivered!"
  end
end
VendingMachine.new.run
